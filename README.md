# Rheocompass converter

A tool to convert exported rheocompass data to usable csv files and json metadata.

# How to use this tool

Look at the [Wiki](https://git.wur.nl/raoul.fix/rheocompass-converter/-/wikis/home) for a detailed guide.
